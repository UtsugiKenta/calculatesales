package jp.alhinc.utsugi_kenta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店情報を保持するためのマップ
		Map<String,String> branchNames = new HashMap<>();
		//支店ごとの売り上げ情報を保持するためのマップ
		Map<String,Long> branchSales = new HashMap<>();

		BufferedReader br = null;

		// 支店定義ファイル読み込みメソッドの呼び出し
		// (パス, ファイル名, ファイルの中身の正規表現, 何の定義ファイルか, 名称情報を保持するマップ, 売上情報を保持するマップ)
		if(!inputFile(args[0], "branch.lst", "^[0-9]{3}$", "支店", branchNames, branchSales)) {
			return;
		}

		//商品定義ファイルを保持するためのマップ
		Map<String,String> commodityNames = new HashMap<>();
		//商品ごとの売り上げ情報を保持するためのマップ
		Map<String,Long> commoditySales = new HashMap<>();

		// 商品定義ファイル読み込みメソッドの呼び出し
		if(!inputFile(args[0], "commodity.lst", "^[a-zA-Z0-9]{8}$", "商品", commodityNames, commoditySales)) {
			return;
		}

		//売り上げファイルの保持
		File[] files = new File(args[0]).listFiles();// パスの中のファイルをすべて取得
		List<File> rcdFiles = new ArrayList<>();// rcdファイルへのパスを持つList

		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);
		//売り上げファイルが連番かどうかの判定
		for(int i = 0; i < rcdFiles.size() -1 ; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売り上げファイルの読み込み
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				String fileName = rcdFiles.get(i).toString();
				File file = new File(fileName);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				List<String> fileDate = new ArrayList<>();// 支店コードと金額を持つリスト

				while((line = br.readLine()) != null) {
					//支店ごとの売り上げファイルの内容保持
					fileDate.add(line);
				}

				// 売り上げファイルのフォーマットがあっているかの判定
				if(fileDate.size() != 3) {// 2から3に変更
					System.out.println(rcdFiles.get(i).getName() +"のフォーマットが不正です");
					return;
				}

				//売り上げファイルに支店コードが入っているかの判定
				if(!branchSales.containsKey(fileDate.get(0))){
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//売り上げファイルに商品コードが入っているかの判定
				if(!commodityNames.containsKey(fileDate.get(1))){
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//売上金額が数字なのか判定
				if(!fileDate.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				// 支店・商品ごとの売上金額の集計
				long fileSales = branchSales.get(fileDate.get(0));// 支店ごとの売り上げ計算
				Long salesAmount = fileSales + Long.parseLong(fileDate.get(2));

				long fileCommodity = commoditySales.get(fileDate.get(1));// 商品ごとの売り上げ計算
				Long commodityAmount = fileCommodity + Long.parseLong(fileDate.get(2));
				//売る上げ合計金額が10桁を超えたかどうか判定
				if(salesAmount >= 10000000000L && commodityAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(fileDate.get(0), salesAmount);
				commoditySales.put(fileDate.get(1), commodityAmount);

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally{
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//支店ごとの売り上げ出力
		if(!outputFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		// 商品ごとの売り上げ出力
		if(!outputFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	// ファイル出力メソッド
	public static boolean outputFile(String filePath, String fileName,
										Map<String, String> namesMap, Map<String, Long> valuesMap) {
		//ファイルごとの集計結果出力
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + valuesMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	// 定義ファイル読み込みメソッド
	public static boolean inputFile(String filePath, String fileName, String pattern, String fileType,
											Map<String,String> namesMap, Map<String,Long> valuesMap ) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, fileName);
			// 定義ファイルが存在しているかの判定
			if(!file.exists()) {
				System.out.println(fileType + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			// 読み込んだファイルの内容を出力
			String line;
			while((line = br.readLine()) != null) {
				// コードと名称を分割して保存
				String[] items = line.split(",");
				// コードがpatternの正規表現かつコードと名称の２点がファイルに記載されているかの判定
				if((items.length != 2) || (!items[0].matches(pattern))){
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}
				// コード・名称の保持
				namesMap.put(items[0], items[1]);
				// コード・売上金額保持
				valuesMap.put(items[0], 0L);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
